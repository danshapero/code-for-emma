{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Get some satellite imagery\n",
    "\n",
    "First and most importantly, making pretty pictures.\n",
    "I do this by visiting the [USGS Earth Explorer](https://earthexplorer.usgs.gov/) website and searching for Landsat-8 imagery near the region I want.\n",
    "This will tell you the paths and rows of the scenes that cross that region.\n",
    "You can then download it from [landsatonaws.com](https://landsatonaws.com) which (unlike USGS or any NASA data centers) doesn't require a password.\n",
    "I picked a scene by scrolling through and looking for one with relatively little cloud cover."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "url = 'http://landsat-pds.s3.amazonaws.com/c1/L8/057/019/LC08_L1TP_057019_20200414_20200423_01_T1/'\n",
    "filename_stem = 'LC08_L1TP_057019_20200414_20200423_01_T1'\n",
    "!wget --no-clobber {url + filename_stem}_B2.TIF\n",
    "!wget --no-clobber {url + filename_stem}_B3.TIF\n",
    "!wget --no-clobber {url + filename_stem}_B4.TIF\n",
    "!wget --no-clobber {url + filename_stem}_B8.TIF"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next we'll read in the panchromatic band (band 8) of the image using the [rasterio](https://rasterio.readthedocs.io/en/latest/) package.\n",
    "The panchromatic band is at 15m resolution, but it's only in black and white.\n",
    "We could show a color image by instead reading in the red, green, and blue bands (4, 3, and 2 respectively), which are recorded at 30m resolution.\n",
    "Creating naturalistic color images requires sophisticated corrections, which you can do using [rio-color](https://github.com/mapbox/rio-color), but which are tedious and annoying."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import rasterio\n",
    "with rasterio.open(f'{filename_stem}_B8.TIF', 'r') as image_file:\n",
    "    bounds = image_file.bounds\n",
    "    img_extent = (bounds.left, bounds.right, bounds.bottom, bounds.top)\n",
    "    image = image_file.read(indexes=1, masked=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And now we can look at it using the `imshow` function from matplotlib.\n",
    "I'm passing an extra argument called `extent` so that the image will be properly georeferenced for when we try to plot more data on top of it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib notebook\n",
    "import matplotlib.pyplot as plt\n",
    "fig, axes = plt.subplots()\n",
    "axes.imshow(image, extent=img_extent, cmap='Greys_r');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Get a thickness map\n",
    "\n",
    "We'll use a thickness map from a [paper](https://www.nature.com/articles/s41561-019-0300-3) by Farinotti et al.\n",
    "Before using this data set, you should know a bit about how they made it.\n",
    "There are no direct measurements of ice thickness of any kind for most of the world's alpine glaciers.\n",
    "But there are good measurements of the surface elevation and flow speed from various remote sensing platforms.\n",
    "Farinotti, Huss, and others have pioneered a technique for estimating the ice thickness from these surface measurements by making some assumptions about the flow regime.\n",
    "This happens to give decent results when compared to ground truth for the few sites where we do have data.\n",
    "But it's just a best guess estimate and it produces weird things which we'll see below.\n",
    "\n",
    "The [data](https://www.research-collection.ethz.ch/handle/20.500.11850/315707) are organized by which region the glacier lives in according to the Randolph Glacier Inventory (RGI).\n",
    "The Juneau Icefield is in RGI region 01."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "url = 'https://www.research-collection.ethz.ch/bitstream/handle/20.500.11850/315707/'\n",
    "filename = 'composite_thickness_RGI60-01.zip'\n",
    "!wget --no-clobber {url + filename}\n",
    "!unzip -n {filename}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next we want to find the data for Taku Glacier.\n",
    "You can find the right IDs by navigating through the [interactive viewer](http://www.glims.org/maps/glims) on the RGI website.\n",
    "Taku Glacier has ID 01390 and Hole-in-the-Wall Glacier has ID 27102.\n",
    "Since Taku and Hole-in-the-Wall are (technically) separate drainages, the Farinotti thickness estimate stores them in different files.\n",
    "The code below will merge these two raster data sets into one using a function in the module `rasterio.merge`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import rasterio.merge\n",
    "glacier_ids = ['01390', '27102']\n",
    "thickness_datasets = [\n",
    "    rasterio.open(f'RGI60-01/RGI60-01.{glacier_id}_thickness.tif', 'r')\n",
    "    for glacier_id in glacier_ids\n",
    "]\n",
    "thickness, transform = rasterio.merge.merge(thickness_datasets)\n",
    "thickness = thickness[0, :, :]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We'll need to write the merged thickness map out to a file for later.\n",
    "Rather than desribe all this in detail I'll refer you to the [rasterio docs](https://rasterio.readthedocs.io/en/latest/quickstart.html)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "profile = {\n",
    "    'driver': 'GTiff',\n",
    "    'height': thickness.shape[0],\n",
    "    'width': thickness.shape[1],\n",
    "    'count': 1,\n",
    "    'dtype': thickness.dtype,\n",
    "    'crs': thickness_datasets[0].crs,\n",
    "    'transform': transform,\n",
    "}\n",
    "\n",
    "with rasterio.open('taku-thickness.tif', 'w', **profile) as dataset:\n",
    "    dataset.write(thickness, indexes=1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The Farinotti estimate for the thickness of Taku goes up to around 900m, which is roughly sort of right.\n",
    "But notice the seam of noisy thickness values between Taku and Hole-in-the-Wall.\n",
    "This is a consequence of how Farinotti et al. computed the thickness separately for each drainage -- it's purely an artifact and if we want to do any modeling we should smooth over this feature."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "xmin, ymax = transform * (0, 0)\n",
    "xmax, ymin = transform * (thickness.shape[1], thickness.shape[0])\n",
    "fig, axes = plt.subplots()\n",
    "axes.set_aspect('equal')\n",
    "axes.set_title('Ice thickness')\n",
    "axes.imshow(image, extent=img_extent, cmap='Greys_r')\n",
    "thk_extent = (xmin, xmax, ymin, ymax)\n",
    "img = axes.imshow(thickness, alpha=0.5, extent=thk_extent, cmap='Blues')\n",
    "fig.colorbar(img, label='meters');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Get a surface elevation map\n",
    "\n",
    "Next we'll get a 10m surface elevation map from [ArcticDEM](https://www.pgc.umn.edu/data/arcticdem/).\n",
    "Two different tiles cover Taku so we'll have to merge them again like we did with the thickness.\n",
    "If need be, you can go nuts and get a 2m resolution map."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "url = 'http://data.pgc.umn.edu/elev/dem/setsm/ArcticDEM/mosaic/v3.0/10m'\n",
    "!wget --no-clobber {url}/40_05/40_05_10m_v3.0.tar.gz\n",
    "!wget --no-clobber {url}/40_06/40_06_10m_v3.0.tar.gz\n",
    "!tar xvf 40_05_10m_v3.0.tar.gz\n",
    "!tar xvf 40_06_10m_v3.0.tar.gz"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "surface_datasets = [\n",
    "    rasterio.open(f'40_{column}_10m_v3.0_reg_dem.tif', 'r')\n",
    "    for column in ['05', '06']\n",
    "]\n",
    "surface, transform = rasterio.merge.merge(surface_datasets)\n",
    "surface = surface[0, :, :]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "profile = {\n",
    "    'driver': 'GTiff',\n",
    "    'height': surface.shape[0],\n",
    "    'width': surface.shape[1],\n",
    "    'count': 1,\n",
    "    'dtype': surface.dtype,\n",
    "    'crs': surface_datasets[0].crs,\n",
    "    'transform': transform,\n",
    "}\n",
    "\n",
    "with rasterio.open('taku-surface-merged.tif', 'w', **profile) as dataset:\n",
    "    dataset.write(surface, indexes=1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we're about to encounter the first annoying thing about geospatial data -- dealing with funny coordinate systems.\n",
    "The surface data use the EPSG:3413 coordinate system, also known as polar stereographic north, while the thickness data are in UTM Zone 8."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(f\"Surface CRS:   {surface_datasets[0].profile['crs']}\")\n",
    "print(f\"Thickness CRS: {thickness_datasets[0].profile['crs']}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So what we'll do next is transform all of the surface data into the coordinate system of the thickness data.\n",
    "Rasterio of course has some functions for [reprojection](https://rasterio.readthedocs.io/en/latest/topics/reproject.html).\n",
    "Rather than do all of that in Python, we can just use a command-line utility for it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!rio warp taku-surface-merged.tif taku-surface.tif --dst-crs EPSG:32608"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with rasterio.open('taku-surface.tif', 'r') as dataset:\n",
    "    transform = dataset.transform\n",
    "    surface = dataset.read(indexes=1, masked=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "xmin, ymax = transform * (0, 0)\n",
    "xmax, ymin = transform * (surface.shape[1], surface.shape[0])\n",
    "fig, axes = plt.subplots()\n",
    "axes.set_aspect('equal')\n",
    "axes.set_title('Ice surface elevation')\n",
    "axes.imshow(image, extent=img_extent, cmap='Greys_r')\n",
    "surf_extent = (xmin, xmax, ymin, ymax)\n",
    "img = axes.imshow(surface, alpha=0.5, vmin=-100, vmax=2000, extent=surf_extent, cmap='Blues')\n",
    "fig.colorbar(img, label='meters');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Get a velocity map\n",
    "\n",
    "Next we'll get some velocity maps for the region from the [ITS_LIVE](https://its-live.jpl.nasa.gov) project."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "url = 'http://its-live-data.jpl.nasa.gov.s3.amazonaws.com/velocity_mosaic/landsat/v00.0/static/cog/'\n",
    "filename_stem = 'ALA_G0120_0000'\n",
    "!wget --no-clobber {url + filename_stem}_vx.tif\n",
    "!wget --no-clobber {url + filename_stem}_vy.tif"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once again the velocity data are in a different CRS and we'll have to reproject."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with rasterio.open(f'{filename_stem}_vx.tif', 'r') as vx_file:\n",
    "    print(f\"Velocity CRS:  {vx_file.profile['crs']}\")\n",
    "    print(f\"Thickness CRS: {thickness_datasets[0].profile['crs']}\")\n",
    "    \n",
    "!rio warp {filename_stem}_vx.tif vx.tif --dst-crs EPSG:32608\n",
    "!rio warp {filename_stem}_vy.tif vy.tif --dst-crs EPSG:32608"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Load in the glacier geometry\n",
    "\n",
    "I hand-digitized an outline of the lower parts of Taku and Hole-in-the-Wall in a GIS and saved it to a GeoJSON file.\n",
    "It's possible that you'll need to hand-edit some of the vertices of the outline in order to avoid a missing data patch, to get more resolution in particular spots, or to extend the domain further upstream or into some of the smaller catchments."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import geojson\n",
    "with open('taku.geojson', 'r') as outline_file:\n",
    "    outline = geojson.load(outline_file)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "fig, axes = plt.subplots()\n",
    "axes.set_aspect('equal')\n",
    "\n",
    "for feature in outline['features']:\n",
    "    for line_string in feature['geometry']['coordinates']:\n",
    "        xs = np.array(line_string)\n",
    "        axes.plot(xs[:, 0], xs[:, 1], linewidth=2)\n",
    "\n",
    "axes.imshow(image, extent=img_extent, cmap='Greys_r')\n",
    "axes.imshow(thickness, alpha=0.5, extent=thk_extent, cmap='Blues');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Icepack includes some helper code for turning this outline into an unstructured triangular mesh.\n",
    "We'll use the mesh generator [gmsh](https://www.gmsh.info) to do this for us.\n",
    "The first command here takes the GeoJSON outline and turns it into the file format that gmsh expects, which has the extension `.geo`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import icepack\n",
    "geometry = icepack.meshing.collection_to_geo(outline)\n",
    "with open('taku.geo', 'w') as geo_file:\n",
    "    geo_file.write(geometry.get_code())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next we'll call gmsh at the command line; you can make command line calls by putting a `!` first. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!gmsh -2 -format msh2 -v 0 -o taku.msh taku.geo "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we'll read this mesh into Firedrake, the finite element modeling package that icepack is built on."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import firedrake\n",
    "mesh = firedrake.Mesh('taku.msh')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And make yet another plot of the mesh.\n",
    "Pay attention to the legend and the outline colors here -- this will be important when we apply boundary conditions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "fig, axes = plt.subplots()\n",
    "axes.set_aspect('equal')\n",
    "firedrake.triplot(mesh, axes=axes)\n",
    "axes.legend();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Interpolate the gridded data to the mesh\n",
    "\n",
    "The next step is to take the gridded data and interpolate it to a new representation on the unstructured mesh.\n",
    "But this begs the question of exactly what that representation will be.\n",
    "We could use functions that are piecewise linear in each triangle, or if we wanted more accuracy we could use piecewise quadratic functions.\n",
    "The information about how you want to represent spatial fields like the thickness or velocity is encapsulated in a *function space* and we create them like so:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Q = firedrake.FunctionSpace(\n",
    "    mesh,\n",
    "    family='CG',\n",
    "    degree=1\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The *family* tells Firedrake what type of element we'll use.\n",
    "In this case, we're doing the simplest thing possible -- the basis consists of piecewise polynomials in each triangle and they're continuous across the triangle boundaries.\n",
    "The `'CG'` stands for continuous Galerkin.\n",
    "There are many other element families.\n",
    "For example, for certain kinds of wave propagation problems we might want elements that are discontinuous across triangle boundaries.\n",
    "\n",
    "The *degree* tells us how high degree of a polynomial we want.\n",
    "In this case we'll just use linear elements.\n",
    "\n",
    "Firedrake has no built-in idea about DEMs, but I've written a helper function in icepack that will interpolate it for us."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with rasterio.open('taku-surface.tif', 'r') as dataset:\n",
    "    s = icepack.interpolate(dataset, Q)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import icepack.plot\n",
    "fig, axes = icepack.plot.subplots()\n",
    "colors = firedrake.tripcolor(s, axes=axes)\n",
    "fig.colorbar(colors);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next we can do the same thing for the ice thickness."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with rasterio.open('taku-thickness.tif', 'r') as dataset:\n",
    "    h_obs = icepack.interpolate(dataset, Q)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import icepack.plot\n",
    "fig, axes = icepack.plot.subplots()\n",
    "colors = firedrake.tripcolor(h_obs, axes=axes)\n",
    "fig.colorbar(colors);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's try to remove that awful seam!\n",
    "What we want is some function $h$ that mostly matches the estimate $h_{\\text{obs}}$.\n",
    "We can measure this by calculating the mean-square misfit between the two."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from firedrake import dx\n",
    "h = firedrake.Function(Q)\n",
    "J_observations = 0.5 * (h - h_obs)**2 * dx"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "But we also want the thickness field we compute to be much smoother.\n",
    "We can quantify the degree of roughness by looking at the magnitude of the gradient of $h$.\n",
    "We'll also need some kind of typical length scale $L$; you can think of this like a smoothing length in a low-pass filter.\n",
    "I'm using 400m here and I'll explain why in a bit."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from firedrake import inner, grad, Constant\n",
    "L = firedrake.Constant(400)\n",
    "J_smoothness = 0.5 * L**2 * inner(grad(h), grad(h)) * dx"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We'll then define an *objective functional* $J$ to be the sum of the model-data misfit and the smoothness criteria and find an $h$ that minimizes the sum of the two.\n",
    "In mathematical notation, the objective functional $J$ is defined as\n",
    "\n",
    "$$J = \\frac{1}{2}\\int_\\Omega\\left((h - h_{\\text{obs}})^2 + L^2|\\nabla h|^2\\right)dx.$$\n",
    "\n",
    "You can see how the code corresponds pretty well to the math: `grad` instead of $\\nabla$, `inner` instead of a dot product, but still more or less identical."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "J = J_observations + J_smoothness\n",
    "F = firedrake.derivative(J, h)\n",
    "bc = firedrake.DirichletBC(Q, h_obs, [2, 3, 4, 5, 6])\n",
    "firedrake.solve(F == 0, h, bc)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, axes = icepack.plot.subplots()\n",
    "colors = firedrake.tripcolor(h, axes=axes)\n",
    "fig.colorbar(colors);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From this plot you can see that we've got rid of the seam but still mostly kept the features of the original estimate.\n",
    "Originally I tried using a smoothing length of 2km but that made everything too diffuse, so I turned $L$ down successively until I found that 400m was about right.\n",
    "We can be much more sophisticated about how we come up with $h$ from the observations, but this should hopefully work for now."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next we'll load in the velocity data.\n",
    "Before, we created a function space for the thickness, but the velocities are a vector field, so we'll created a vector function space."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "V = firedrake.VectorFunctionSpace(mesh, 'CG', 1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can interpolate the gridded data sets to the vector function space just like we did before, but we instead pass the two files as a tuple."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with rasterio.open('vx.tif', 'r') as vx_file, \\\n",
    "     rasterio.open('vy.tif', 'r') as vy_file:\n",
    "    raw_data = icepack.interpolate((vx_file, vy_file), V)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "More coordinate system weirdness!\n",
    "The polar stereographic projection that the velocity data came from originally is not only translated but also rotated with respect to the transverse Mercator projection that we're using.\n",
    "If we're just reprojecting a scalar field then that doesn't matter, but for a vector field we also have to rotate the components.\n",
    "Luckily it's a factor of 90${}^\\circ$ so this is easy to do\\*.\n",
    "We just need to use the Firedrake function `as_vector` to create a vector expression; we can access the components of the raw data vector field using the usual array access notation.\n",
    "\n",
    "\\*I think it's 90${}^\\circ$ and the picture looks right but we should *probably* double check this."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from firedrake import as_vector\n",
    "rotated_data = as_vector((-raw_data[1], raw_data[0]))\n",
    "u_obs = firedrake.interpolate(rotated_data, V)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The streamlines go from thinner at the start to thicker at the end, which is a nice sanity check for the plot below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, axes = icepack.plot.subplots()\n",
    "streamlines = firedrake.streamplot(u_obs, resolution=100.0, seed=1729, axes=axes)\n",
    "fig.colorbar(streamlines);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To help us come up with a sane value of the friction coefficient later, we'll calculate the driving stress."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from icepack.constants import ice_density as ρ_I, gravity as g\n",
    "τ = firedrake.project(-ρ_I * g * h * grad(s), V)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, axes = icepack.plot.subplots()\n",
    "colors = firedrake.tripcolor(τ, vmax=0.3, axes=axes)\n",
    "axes.set_title('Driving stress')\n",
    "fig.colorbar(colors, label='megapascals');"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "firedrake",
   "language": "python",
   "name": "firedrake"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
